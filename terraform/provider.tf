# PROVIDER

provider "aws" {
  region  = var.region
  version = "~> 3.11"
}

terraform {
  required_version = "= 0.13.5"
}