# TODO : Create a s3 bucket with aws_s3_bucket
resource "aws_s3_bucket" "s3_bucket_cfy" {
  bucket = var.s3_user_bucket_name
  acl    = "private"
  force_destroy = true
}

# TODO : Create 1 nested folder :  job_offers/raw/  |  with  aws_s3_bucket_object
resource "aws_s3_bucket_object" "object" {
  bucket = aws_s3_bucket.s3_bucket_cfy.id
  key    = "job_offers/raw/"
  source = "/dev/null"
}

# TODO : Create an event to trigger the lambda when a file is uploaded into s3 with aws_s3_bucket_notification
resource "aws_s3_bucket_notification" "bucket_notification" {
  bucket = aws_s3_bucket.s3_bucket_cfy.id

  lambda_function {
    lambda_function_arn = aws_lambda_function.lambda_function.arn
    events              = ["s3:ObjectCreated:*"]
    filter_prefix       = "job_offers/raw/"
    filter_suffix       = ".csv"
  }

  depends_on = [aws_lambda_permission.allow_bucket]
}